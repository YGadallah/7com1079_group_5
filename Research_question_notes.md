---
title: Research Notes
---

## Questions App Dataset:
### Link : [https://www.kaggle.com/lava18/google-play-store-apps/download#googleplaystore_user_reviews.csv](https://www.kaggle.com/gauthamp10/google-playstore-apps#Google-Playstore-Full.csv)

To what extend does rating influence the downloading of an application?

* What the ratio is between downloads and reviews? Do people care about reviews?
* What is the msot used/demanded category/app in play store?
* Why are the most downloaded apps the most downloaded apps?
* What are the common factors that affect people in order for them to download apps?


* Are people more inclined towards paying for an application over another based on category?
* Are people keen to review an app? what takes people to review an app?
	* Are people more inclined to rate an application based on their experience being positive or negative?
	* Which category is the most rated or has more reviews?
		* Do application characteristics impact the choice of users to download applications?
		* How have the economic and social factors behind being the most downloaded apps?
		* How have economic, cultural and social factors behind being the most downloaded apps? Do application characteristics impact user's choice?
		* Do users choose applications based on features even if the application defies their economic, cultural or social status?
		
## Question Food/Diet/Lifestyle dataset:
### Link : [https://www.kaggle.com/jboysen/global-food-prices](https://www.kaggle.com/jboysen/global-food-prices)

* To what extent do economic, cultural and geographical factors affect the consumer's nutritional habits?
//in differenct geographical locations
	* Health related issues
	* Nutritional deficiencies
	

## Resources:
1. [https://www.scribbr.com/research-process/research-question-examples/](https://www.scribbr.com/research-process/research-question-examples/)
2. [https://writingcenter.gmu.edu/guides/how-to-write-a-research-question](https://writingcenter.gmu.edu/guides/how-to-write-a-research-question)