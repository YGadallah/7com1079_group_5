This it the Bitbucket Repo for Group 5 for the module 7COM1079

The students on this module are:
Student IDs:
18035891
18050884
18052749
19006181
18060508

Bitbucket: 
git clone https://YGadallah@bitbucket.org/YGadallah/7com1079_group_5.git
Trello:
https://trello.com/b/OTJzzji5/7com1079-project-planning-lab

All the files including the SPSS dataset with the output files, cleaned dataset, raw dataset and all the uncompiled pieces of work from everyone involved are in this repository.
All SPSS work was done together on one machine, all report writing was done seperately and compiled into one report on one machine at the end.